<?php

function give_user_subscription( $product, $user_id, $note = '', $discount = '' ){

	if( ! function_exists( 'wc_create_order' ) || ! function_exists( 'wcs_create_subscription' ) || ! class_exists( 'WC_Subscriptions_Product' ) ){
		return false;
	}

	$order = wc_create_order( array( 'customer_id' => $user_id ) );

	if( is_wp_error( $order ) ){
		return false;
	}

	$user = get_user_by( 'ID', $user_id );

	$fname     = $user->first_name;
	$lname     = $user->last_name;
	$email     = $user->user_email;
	$address_1 = get_user_meta( $user_id, 'billing_address_1', true );
	$address_2 = get_user_meta( $user_id, 'billing_address_2', true );
	$city      = get_user_meta( $user_id, 'billing_city', true );
	$postcode  = get_user_meta( $user_id, 'billing_postcode', true );
	$country   = get_user_meta( $user_id, 'billing_country', true );
	$state     = get_user_meta( $user_id, 'billing_state', true );

	$address         = array(
		'first_name' => $fname,
		'last_name'  => $lname,
		'email'      => $email,
		'address_1'  => $address_1,
		'address_2'  => $address_2,
		'city'       => $city,
		'state'      => $state,
		'postcode'   => $postcode,
		'country'    => $country,
	);

	$order->set_address( $address, 'billing' );
	$order->set_address( $address, 'shipping' );
	$order->add_product( $product, 1 );
	$sub = wcs_create_subscription(array(
		'order_id' => $order->get_id(),
		'status' => 'pending',
		'billing_period' => \WC_Subscriptions_Product::get_period( $product ),
		'billing_interval' => \WC_Subscriptions_Product::get_interval( $product )
	));

	if( is_wp_error( $sub ) ){
		return false;
	}

	$start_date = gmdate( 'Y-m-d H:i:s' );
	$sub->add_product( $product, 1 );

	$dates = array(
		'trial_end'    => \WC_Subscriptions_Product::get_trial_expiration_date( $product, $start_date ),
		'next_payment' => \WC_Subscriptions_Product::get_first_renewal_payment_date( $product, $start_date ),
		'end'          => \WC_Subscriptions_Product::get_expiration_date( $product, $start_date ),
	);
	
	if($discount != '') {
		give_user_discount($sub->get_id(), 'Desconto da Queropago', $discount);
	}
	
	$sub->update_dates( $dates );
	$sub->calculate_totals();
	//$order->calculate_totals();
	$installment_id = wcs_create_renewal_order($sub);

	// Update order status with custom note
	$note = ! empty( $note ) ? $note : __( 'Encomenda e assinatura inseridas via QueroPago.' );
	//$order->update_status( 'on-hold', $note, true );
	// Also update subscription status to active from pending (and add note)
	//$sub->update_status( 'active', $note, true );

	return $sub;
}

function give_user_discount( $order_id, $title, $amount, $tax_class = '' ) {
    $order    = wc_get_order($order_id);
    $subtotal = $order->get_subtotal();
    $item     = new \WC_Order_Item_Fee();

    if ( strpos($amount, '%') !== false ) {
        $percentage = (float) str_replace( array('%', ' '), array('', ''), $amount );
        $percentage = $percentage > 100 ? -100 : -$percentage;
        $discount   = $percentage * $subtotal / 100;
    } else {
        $discount = (float) str_replace( ' ', '', $amount );
        $discount = $discount > $subtotal ? -$subtotal : -$discount;
    }

    $item->set_tax_class( $tax_class );
    $item->set_name( $title );
    $item->set_amount( $discount );
    $item->set_total( $discount );

    if ( '0' !== $item->get_tax_class() && 'taxable' === $item->get_tax_status() && wc_tax_enabled() ) {
        $tax_for   = array(
            'country'   => $order->get_shipping_country(),
            'state'     => $order->get_shipping_state(),
            'postcode'  => $order->get_shipping_postcode(),
            'city'      => $order->get_shipping_city(),
            'tax_class' => $item->get_tax_class(),
        );
        $tax_rates = \WC_Tax::find_rates( $tax_for );
        $taxes     = \WC_Tax::calc_tax( $item->get_total(), $tax_rates, false );
        print_pr($taxes);

        if ( method_exists( $item, 'get_subtotal' ) ) {
            $subtotal_taxes = \WC_Tax::calc_tax( $item->get_subtotal(), $tax_rates, false );
            $item->set_taxes( array( 'total' => $taxes, 'subtotal' => $subtotal_taxes ) );
            $item->set_total_tax( array_sum($taxes) );
        } else {
            $item->set_taxes( array( 'total' => $taxes ) );
            $item->set_total_tax( array_sum($taxes) );
        }
        $has_taxes = true;
    } else {
        $item->set_taxes( false );
        $has_taxes = false;
    }
    $item->save();

    $order->add_item( $item );
    $order->calculate_totals( $has_taxes );
    $order->save();
}