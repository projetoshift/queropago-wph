<?php

error_reporting(-1);
/**
 * Plugin Name:     QueroEducação - Integração Wordpress e WooCommerce
 * Plugin URI:      https://wp-helpers.com/integrations/queropago
 * Description:     Integração das APIs e sistemas da QueroEducação e QueroPago com a interface e plataforma do Wordpress e WooCommerce.
 * Author:          WP Helpers | Carlos Matos
 * Author URI:      https://wp-helpers.com
 * Text Domain:     queropago-wph
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package         QPWPH
 */

if ( ! defined( 'ABSPATH' ) ) { exit; }

define('QUEROPAGO_BASE', __FILE__);
define('QUEROPAGO_SLUG', 'queropago-wph');
define('QUEROPAGO_PREFIX', 'queropago_');
define('QUEROPAGO_VERSION', '1.0.0');

use QPWPH\Init;

/**
 * Aciona o autoload via Composer 2.0.8.
 */
require __DIR__ . '/vendor/autoload.php';
require 'helpers.php';

/**
 * Instancia classe de startup do plugin.
 */
Init::instance();