<?php

namespace QPWPH;

/**
 * Gera os pedidos automaticamente para os usuários inseridos ou
 * atualizados por parte da Queropago.
 * 
 * @since 	1.0.0
 * @package QPWPH
 * @access  public
 *
 * @see 	Api\Callbacks::enrollmentCreated($data)
 * @see 	\WC_Order()
 */
class Orders {

	public function __construct() {

		add_filter('woocommerce_order_number', array($this, 'orderPrefix'));
		add_action('init', array($this, 'addCustomStatus'));
		add_filter('wc_order_statuses', array($this, 'addOrderStatus'));
		add_action('admin_head', array($this, 'orderCSS'));
	}

	public function generateOrder($userdata, $user_id, $orderdata, $coursedata) {

		do_action(QUEROPAGO_PREFIX . 'before_create_order');

		$subscription = $this->isSubscription($orderdata['payment_methods']['installments']);

		$orders = new \WC_Order();
		$orders->set_address( $address, 'billing' );			
		$orders->add_product(get_product(19), 1);
		$orders->calculate_totals();
		$orders->update_status( 'completed', 'Order created dynamically - ', TRUE);

		$order_id = $order->get_id();
		$this->orderPrefix($order_id);


		do_action(QUEROPAGO_PREFIX . 'after_create_order');

	}
	/**
	 * Verifica se a matrícula envolve uma assinatura ou consiste de pagamento à vista
	 *
	 * @since 	1.0.0
	 * @access 	public
	 * @param 	$enrollment 	int 	Número de parcelas fornecido pela Queropago.
	 *
	 * @return 	boolean()
	 */
	public function isSubscription($enrollment) {

		if($enrollment != 1) {
			return true;
		}

		return false;
	}

	/**
	 * Cria IDs de pedidos específicos para vendas feitas pela QueroPago
	 *
	 * @since 	1.0.0
	 * @access 	protected
	 * @param 	$order_id 	int 	ID do pedido.
	 *
	 * @return 	string 	$order_id ou prefixo.
	 */
	public function orderPrefix($order_id) {
		
		$order = wc_get_order($order_id);
		$items = $order->get_items(); 
		foreach ( $items as $item_id => $item ) {
		   $product_id = $item->get_variation_id() ? $item->get_variation_id() : $item->get_product_id();
		   if (($product_id === 12) || ($product_id === 19) ) {
			   return 'QP-' . $order_id;
		   }
		}
		
		return $order_id;
	}
	
	public function addCustomStatus() {
		register_post_status( 'wc-qp-enrolled', array(
			'label'                     => 'Matrícula Queropago',
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'Matrícula Queropago (%s)', 'Matrícula Queropago (%s)' )
		) );
	}
	
	public function addOrderStatus($order_statuses) {
		
		$new_order_statuses = array();
		
		foreach ( $order_statuses as $key => $status ) {
			$new_order_statuses[ $key ] = $status;
			$new_order_statuses['wc-qp-enrolled'] = 'Matrícula Queropago';
    	}
 
    	return $new_order_statuses;
	}
	
	public function orderCSS() {
		?>
			<style>
			mark.order-status.status-qp-enrolled { background: #fa6400; color: white; font-weight: bold; font-size: 0.9em;}
			</style>
		<?php
	}
}