<style>
#wpfooter {display: none;}
</style>

<div class="wrap container-fluid">
	<div style="height: 20px;"></div>
    	<div class="row">
        <div class="col-md-8">
        	<h4 class="display-4" style="font-size: 2em;">Queropago - Inglês de Verdade</h4>
        </div>
        <div class="col-md-4">
        	<p class="description text-right">
            Configurações e ferramentas relacionadas à parceria com a QueroPago e QueroEducação.
            </p>
        </div>
        <hr>
        </div>

<div style="height: 20px;"></div>

<div class="col-md-12">

    <div class="alert alert-success" role="alert">

        <h4 class="alert-heading">Bem-vindo!</h4>
        <p>Evite modificar essas configurações sem suporte ou qualquer apoio - em geral, essas configurações estão na dependência da QueroPago e dos parâmetros relacionados à API e às requisições realizadas pela empresa.</p>
    <hr>
		<p class="mb-0">No caso de dúvidas, entre em contato conosco.</p>
</div>
<div style="height: 20px;"></div>



<div class="wrap cmb2-options-page option-<?php echo $hookup->option_key; ?>">

                <form class="cmb-form" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="POST" id="<?php echo $hookup->cmb->cmb_id; ?>" enctype="multipart/form-data" encoding="multipart/form-data">
                    <input type="hidden" name="action" value="<?php echo esc_attr( $hookup->option_key ); ?>">
                    <?php $hookup->options_page_metabox(); ?>
                    <nav class="navbar fixed-bottom navbar-light bg-light">
		                <ul class="navbar-nav ml-auto">
            		    <li class="nav-item">
                        <?php submit_button( esc_attr( $hookup->cmb->prop( 'save_button' ) ), 'primary', 'submit-cmb' ); ?>
                        </li>
  		                </ul>
  
	               </nav>
                </form>
            </div>
        </div>
            
        <script>
        // Wordpress button style override
        var element = document.getElementById("submit-cmb"); 
        element.classList.remove("button","button-primary");
        element.classList.add("btn","btn-success");
        </script>

