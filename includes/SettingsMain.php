<?php

namespace QPWPH;

class SettingsMain extends Settings {

	public function __construct() {
        
        parent::__construct(
        	array(
				'id' => QUEROPAGO_PREFIX . 'control',
                'title' => __('Queropago', QUEROPAGO_SLUG),
                'description' => __('Configurações da parceria Queropago.', QUEROPAGO_SLUG),
				'object_types' => array( 'options-page' ),
                'classes' => array('col-md-12', 'form-group'),
                'cmb_styles' => false,
				'option_key' =>QUEROPAGO_PREFIX . '_options',
                'parent_slug' => 'woocommerce',
                'capability' => 'manage_options',
                'display_cb' => array($this, 'page'),
                'save_button' => esc_html__( 'Salvar Configurações', QUEROPAGO_SLUG),
			),
			array(
				array(
                    'name' => __( '<strong>Webhook Secret</strong>', QUEROPAGO_SLUG),
                    'description' => __( 'Fornecido pela Queropago para requisições reversas e sua autenticação.', QUEROPAGO_SLUG),
                    'id'   => QUEROPAGO_PREFIX . 'secret',
                    'type' => 'text',
                    'classes' => 'col-md-6',
                    'before_row' => '<div class="row">',                
                ),
                array(
                    'name' => __( '<strong>Limite de Desconto</strong>', QUEROPAGO_SLUG),
                    'description' => __( '<hr>Máximo de desconto permitido a matrículas da Queropago.', QUEROPAGO_SLUG),
                    'id'   => QUEROPAGO_PREFIX . 'desconto',
                    'type' => 'select',
                    'default' => '30',
                    'options' => array(
                        '10' => __('Desconto máximo de 10%', QUEROPAGO_SLUG),
                        '20' => __('Desconto máximo de 20%', QUEROPAGO_SLUG),
                        '30' => __('Desconto máximo de 30%', QUEROPAGO_SLUG),
                        '40' => __('Desconto máximo de 40%', QUEROPAGO_SLUG),
                        '50' => __('Desconto máximo de 50%', QUEROPAGO_SLUG)
                    ),
                    'classes' => 'col-md-6',
                    'after_row' => '</div>',                
                ),
			)
        );
    }

    public function page($hookup) {
    	include __DIR__ . '/templates/main.php';
    }
}