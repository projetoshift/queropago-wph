<?php

namespace QPWPH;

class SubscriptionsMeta extends Settings {

	public function __construct() {
        
        parent::__construct(
        	array(
				'id' => QUEROPAGO_PREFIX . 'subs',
                'title' => __('Queropago', QUEROPAGO_SLUG),
                'description' => __('Dados adicionais da assinatura.', QUEROPAGO_SLUG),
				'object_types' => array( 'shop_subscription' ),
				'context'       => 'normal',
            	'priority'      => 'high',
				'context'		=> 'side',
            	'show_names'    => true
			),
			array(
				array(
                    'name' => __( '<strong>Código da Matrícula</strong>', QUEROPAGO_SLUG),
                    'description' => __( 'Gancho para rastreamento das ações.', QUEROPAGO_SLUG),
                    'id'   => QUEROPAGO_PREFIX . 'enrol_code',
                    'type' => 'text'                
                ),
			)
        );
    }

}