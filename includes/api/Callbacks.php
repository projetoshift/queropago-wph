<?php

namespace QPWPH\Api;
use QPWPH\Api\Cidades;

class Callbacks {

	/**
	 * Função de processamento da webhook enrollment_created
	 *
	 * @since 	1.0.0
	 * @param 	$data 	array 	Body da chamada POST da Queropago.
	 * @see 	Init()->process
	 *
	 * HOOKS
	 * =====
	 * -> queropago_before_create_user
	 * -> queropago_after_create_user($userdata, $user_id, $orderdata, $coursedata)
	 *
	 * @return 	$obs 	string 	Mensagem de retorno da requisição.
	 */
	public static function enrollmentCreated($data) {

		do_action(QUEROPAGO_PREFIX . 'before_create_user');
		
		// Recupera configuração de nível máximo de desconto.
		$conditions = cmb2_get_option(QUEROPAGO_PREFIX . '_options', QUEROPAGO_PREFIX . 'desconto');
		$max_desc = intval($conditions);

		$json = json_decode($data, true);
		$userdata = $json['student'];
		$orderdata = $json['bills'];
		$coursedata = $json['course'];
		$type = $json['period_installments'];
		$discount = strval($json['discount_percentage']) . '%';
		$enrol_id = $json['external_id'];

		// Se o desconto excede o máximo, retorna erro e impede a criação da matrícula.
		if(intval($json['discount_percentage']) > $max_desc) {
			return __('Nível de desconto excede o permitido.', QUEROPAGO_SLUG);
		}

		$firstname = strstr($userdata['name'], ' ', true);
		$lastname = str_replace($firstname, '', $userdata['name']);

		$user_exists = get_user_by('email', $userdata['email']);

		if($user_exists) {

			$user_id = $user_exists->ID;

			wp_update_user( array(
				'ID' => $user_id,
				'display_name' => $userdata['name']
			));
			
			$address = $userdata['address'];
			$local = $address['city']['ibge_code'];
			$geo = new Cidades();

			$customer = new \WC_Customer($user_id);
			$customer->set_billing_phone($userdata['cellphone']);
			$customer->set_billing_address($address['street'] . ',' . $address['number']);
			$customer->set_billing_address_2($address['neighborhood'] . ' | ' . $address['complement']);
			$customer->set_billing_postcode($address['postal_code']);
			$customer->set_billing_city($geo->cities[$local]['cidade']);
			$customer->set_billing_state($geo->cities[$local]['cidade']);

			$customer->save();

			if($type === 1) {
				$product = wc_get_product(19);
			} elseif($type === 12) {
				$product = wc_get_product(12);
			}
			
            if(wcs_get_users_subscriptions($user_id, 12) || wcs_get_users_subscriptions($user_id, 19)){
               	$obs = 'Usuário já existente no site e já é assinante.';
				return $obs;
            } else {
				$sub = give_user_subscription($product, $user_id, 'Matrícula criada', $discount);
				update_post_meta($sub->get_id(), QUEROPAGO_PREFIX . 'enrol_code', $enrol_id);
				//wcs_create_renewal_order($sub);
				$obs = 'Usuário já existente no site. Dados atualizados.';
				return $obs;
			}

		} else {

			$user_id = wp_insert_user( array(
				'user_login' => $userdata['email'],
				'user_email' => $userdata['email'],
				'display_name' => $userdata['name'],
				'first_name' => $firstname,
				'last_name' => $lastname		
			));

			$address = $userdata['address'];
			$local = $address['city']['ibge_code'];
			$geo = new Cidades();

			$customer = new \WC_Customer($user_id);
			$customer->set_billing_phone($userdata['cellphone']);
			$customer->set_billing_address($address['street'] . ',' . $address['number']);
			$customer->set_billing_address_2($address['neighborhood'] . ' | ' . $address['complement']);
			$customer->set_billing_postcode($address['postal_code']);
			$customer->set_billing_city($geo->cities[$local]['cidade']);
			$customer->set_billing_state($geo->cities[$local]['cidade']);

			$customer->save();
			
			if($type === 1) {
				$product = wc_get_product(19);
			} elseif($type === 12) {
				$product = wc_get_product(12);
			}
			
			$sub = give_user_subscription($product, $user_id, 'Matrícula criada', $discount);
			update_post_meta($sub->get_id(), QUEROPAGO_PREFIX . 'enrol_code', $enrol_id);

			$obs = 'Usuário criado no site.';
			return $obs;
		}

		do_action(QUEROPAGO_PREFIX . 'after_create_user');
	}

	
	/**
	 * Função de processamento da webhook enrollment_canceled
	 *
	 * @since 	1.0.0
	 * @param 	$data 	array 	Body da chamada POST da Queropago.
	 * @see 	Init()->process
	 *
	 * HOOKS
	 * =====
	 * -> queropago_before_create_user
	 * -> queropago_after_create_user
	 *
	 * @return 	$obs 	string 	Mensagem de retorno da requisição.
	 */
	public static function enrollmentCanceled($data) {
		
		do_action(QUEROPAGO_PREFIX . 'before_cancel_subscription');
		
		$json = json_decode($data, true);
		$enrol_id = $json['external_id'];
		
		$args = array(
			'post_type' => 'shop_subscription',
			'numberposts' => 1,
			'post_status' => 'wc-active',
			'meta_key' => QUEROPAGO_PREFIX . 'enrol_code',
			'meta_value' => $enrol_id,
			'fields' => 'ids'
		);
		
		$sub = get_posts($args);
		if(!$sub) {
			return __('Matrícula inexistente no sistema.', QUEROPAGO_SLUG);
		}
		$subscription_id = $sub[0];
		
		$subscription = wcs_get_subscription($subscription_id);
		
		try {
			if ( ! $subscription->has_status( wcs_get_subscription_ended_statuses() ) ) {
				$subscription->cancel_order();
			}
		} catch ( Exception $e ) {
			$subscription->add_order_note( sprintf( __( 'Falha no cancelamento do pedido.', QUEROPAGO_SLUG ), is_object( $order ) ? $order->get_order_number() : $order, $e->getMessage() ) );
		}
		
		if($subscription->get_status() === 'cancelled') {
			return __('Matrícula cancelada com sucesso', QUEROPAGO_SLUG);
		} elseif($subscription->get_status() === 'pending-cancel') {
			return __('Cancelamento aguarda confirmação no sistema', QUEROPAGO_SLUG);
		} else {
			return __('Não foi possível cancelar a matrícula', QUEROPAGO_SLUG);
		}
		
		do_action(QUEROPAGO_PREFIX . 'after_cancel_subscription');
	}

	public static function billCreated($data) {
		
		do_action(QUEROPAGO_PREFIX . 'before_create_payment');
		
		$json = json_decode($data, true);
		$enrol_id = $json['enrollment']['external_id'];
		$due_to = $json['due_date'];
		
		$args = array(
			'post_type' => 'shop_subscription',
			'numberposts' => 1,
			'post_status' => 'wc-active',
			'meta_key' => QUEROPAGO_PREFIX . 'enrol_code',
			'meta_value' => $enrol_id,
			'fields' => 'ids'
		);
		
		$sub = get_posts($args);
		$subscription_id = $sub[0];
		$subscription = wcs_get_subscription($subscription_id);
		
		try {
        	$next = \DateTime::createFromFormat('Y-m-d', $due_to);
        	update_post_meta( $subscription_id , '_schedule_next_payment', $next->format('Y-m-d H:i:s'));
			$installment_id = wcs_create_renewal_order($subscription);
			return __('Data do próximo vencimento atualizada.', QUEROPAGO_SLUG);
		} catch ( Exception $e ) {
			$subscription->add_order_note( sprintf( __( 'Próximo vencimento não foi alterado.', QUEROPAGO_SLUG ), is_object( $order ) ? $order->get_order_number() : $order, $e->getMessage() ) );
			return __('Não foi possível atualizar o vencimento', QUEROPAGO_SLUG);
		}
		
	}

	public static function billPaid($data) {
		
		do_action(QUEROPAGO_PREFIX . 'before_confirm_payment');
		
		$json = json_decode($data, true);
		$enrol_id = $json['enrollment']['external_id'];
		$paid_code = $json['external_id'];
		
		$args = array(
			'post_type' => 'shop_subscription',
			'numberposts' => 1,
			'post_status' => 'wc-active',
			'meta_key' => QUEROPAGO_PREFIX . 'enrol_code',
			'meta_value' => $enrol_id,
			'fields' => 'ids'
		);
		
		$sub = get_posts($args);
		$subscription_id = $sub[0];
		
		$subscription = new \WC_Subscription($subscription_id);
		$related = $subscription->get_related_orders();
		foreach($related as $payable) {
			$is_payable = wc_get_order($payable);
			if($is_payable->get_status() === 'pending') {
				$is_payable->update_status('completed');
				$is_payable->add_order_note(sprintf('Mensalidade paga - código na Queropago: %s.', $paid_code));
				return __('Pedido concluído no site e pagamento confirmado.', QUEROPAGO_SLUG);
			}
		}
		return __('Não há vencimentos criados para este pagamento.', QUEROPAGO_SLUG);
	}

	public static function billOverdue($data) {}

	public static function billChanged($data) {}

}
