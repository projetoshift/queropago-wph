<?php

namespace QPWPH\Api;

class Config {

	protected $restPrefix = 'wp-json';

	public function __construct() {

		add_filter('rest_url_prefix', array($this, 'getRestPrefix'));
	}

	public function setRestPrefix(string $slug) {

		$this->restPrefix = $slug;
	}

	public function getRestPrefix() {

		return $this->restPrefix;
	}
}