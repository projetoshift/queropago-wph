<?php

namespace QPWPH;

/**
 * Insere novos campos ao perfil de usuários e lida com inserção, atualização e
 * tarefas ligadas à gestão de usuários.
 * 
 * @since 	1.0.0
 * @package QPWPH
 * @access  public
 *
 * @see 	Api\Callbacks::enrollmentCreated($data)
 * @see 	Utilizada no callback específico de matrícula criada, com o método add()
 */
class Users {

	public function __construct() {

		$this->fields();
	}
	/**
	 * Adiciona campos personalizados ao perfil dos usuários, para abrigar dados vindos da Queropago.
	 * 
	 * @todo 	Escolher manipulador de usermeta.
	 */
	public function fields() {

	}
	/**
	 * Adiciona usuário vindo de request da Queropago ao endpoint, sob o método enrollmentCreated()
	 *
	 * @since 	1.0.0
	 * @access 	public
	 * @static 
	 *
	 * @param 	$userdata 	array 	Array de dados do estudante matriculado.
	 * @return 	void()
	 */
	public static function add(array $userdata) {

		$display_name = $userdata['name'];
		$user_mail = $userdata['email'];
		$login = $userdata['identity_card'];

		$cpf = $userdata['cpf'];
		$phone = $userdata['cellphone'];
		$qp_ref = $userdata['id'];

		$used_id = wp_insert_user();
	}

}