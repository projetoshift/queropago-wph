<?php

namespace QPWPH;

/**
 * Insere ou atualiza dados dos usuários no contexto de cliente do Woocommerce.
 * 
 * @since 	1.0.0
 * @package QPWPH
 * @access  public
 *
 * @see 	Api\Callbacks::enrollmentCreated($data)
 * @see 	\WC_Customer()
 */
class Customers {

	public static function insertData($userdata, $user_id, $orderdata, $coursedata) {

		$address = $userdata['address'];

		$customer = new \WC_Customer($user_id);
		$customer->set_billing_phone($userdata['cellphone']);
		$customer->set_billing_address($userdata['street'] . ',' . $userdata['number']);
		$customer->set_billing_address_2($userdata['neighborhood'] . ' | ' . $userdata['complement']);
		$customer->set_billing_postcode($userdata['postal_code']);

		$customer->save();
	}
}