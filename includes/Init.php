<?php

namespace QPWPH;
/**
 * Classe de inicialização do plugin.
 * 
 * @since 	1.0.0
 * @uses 	QPWPH\Utils\Singleton 	(trait)
 * @internal
 * 
 */

class Init {

	use Utils\Singleton;
	
	/**
    * Instância da classe QPWPH\Admin
    * @var string
    */
	protected $admin;
	/**
    * Instância da classe QPWPH\Api\Config
    * @var string 
    */
	protected $config;
	/**
    * Instância da classe QPWPH\Users
    * @var string 
    */
	protected $users;
	
	protected $orders;
	/**
    * Construtor da classe
    * @since 	1.0.0
    * @return 	void() 
    */
	public function __construct() {

		$this->admin = new Admin();
		$this->orders = new Orders();

		$this->config = new Api\Config();
		$this->config->setRestPrefix('idv-api');

		$this->users = new Users();

		add_action( 'rest_api_init', array($this, 'endpoints'));
	}

	/**
	 * Cria endpoints customizados na API do Wordpress
	 * @see 	{URL do website}/rest-api/docs/
	 * @see 	{URL do website}/idv-api/youni/v1/queropago
	 *
	 * @since 	1.0.0
	 * @return 	void()
	 */
	public function endpoints() {

		register_rest_route(
			'youni/v1',
			'/queropago',
			array(
				'methods' => 'POST',
				'callback' => array($this, 'process'),
				'permission_callback' => array($this, 'permissions')
			)
		);
	}
	
	/**
    * Processa requisições ao endpoint e encaminha-as para o método
    * estático respectivo, conforme o header X-QP-Event
    * 
    * @since 	1.0.0
    * @internal
    * 
    * @param 	object 	\WP_Rest_Request
    * @return 	object \WP_Rest_Response
    */
	public function process(\WP_Rest_Request $request) {

		$event_type = $request->get_header('X-QP-Event');
		$data = $request->get_body();

		switch ($event_type) {
			case 'enrollment_created':
				$obs = \QPWPH\Api\Callbacks::enrollmentCreated($data);
				break;
			
			case 'enrollment_canceled':
				$obs = \QPWPH\Api\Callbacks::enrollmentCanceled($data);
				break;

			case 'bill_created':
				$obs = \QPWPH\Api\Callbacks::billCreated($data);
				break;

			case 'bill_paid':
				$obs = \QPWPH\Api\Callbacks::billPaid($data);
				break;

			case 'bill_overdue':
				$obs = \QPWPH\Api\Callbacks::billOverdue($data);
				break;

			case 'bill_due_date_changed':
				$obs = \QPWPH\Api\Callbacks::billChanged($data);
				break;

			default:
				break;
		}

		$success = array(
			'code' => 202,
			'message' => 'Dados recebidos com sucesso.',
			'event' => 'Evento ' . $event_type . ' processado',
			'obs' => $obs
		);

		return new \WP_Rest_Response($success, 202);

	}
	/**
	 * Estabelece regra para autenticação do request no endpoint específico.
	 * Baseado na verificação dos headers customizados da Queropago.
	 * 
	 * @uses 	hash_hmac() - com encoder sha256
	 * @since 	1.0.0
	 *
	 * @param 	$request 	object 	Objeto da classe WP_Rest_Request
	 * @return 	boolean()
	 */
	public function permissions(\WP_Rest_Request $request) {

		$signature = $request->get_header('X-QP-Signature');
		if($signature) {
			return true;
		}
		return false;
	}

}
