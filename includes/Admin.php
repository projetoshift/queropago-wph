<?php

namespace QPWPH;

class Admin {

	protected $settings_main;
	
	protected $subscriptions;

	public $hooks = [
		'woocommerce_page_queropago__options'
	];

	public function __construct() {

		$this->settings_main = new SettingsMain();
		$this->subscriptions = new SubscriptionsMeta();

		add_action('admin_enqueue_scripts', array($this, 'enqueue_styles'));
        add_action('admin_enqueue_scripts', array($this, 'enqueue_scripts'));
	}

	/**
	 * Registra arquivos CSS para o backend.
	 * @since    1.0.0
	 * 
	 * @param	$hook	String	Hook da página, em caso de restrição no registro da folha de estilo.
	 */
    public function enqueue_styles($hook) {
		
		if ( !in_array($hook, $this->hooks)) {
			return;
		}
		wp_enqueue_style( 'bootstrap-admin', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' );

	}

	/**
	 * Registra arquivos Javascript para o backend.
	 * @since    1.0.0
	 * 
	 * @param	$hook	String	Hook da página, em caso de restrição no registro da biblioteca.
	 */
	public function enqueue_scripts($hook) {

		if ( $hook != ('caravelas-central_page_caravelas__options') ) {
			return;
		}

		wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', null, null, true );
		wp_enqueue_script( 'bootstrap-main', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', null, null, true );

	}

}